package main

import (
	"encoding/json"
	"net/http"
	"surl/internal/app"
	"surl/internal/domain"
	"surl/internal/domain/short_url"
	"surl/internal/infrasturcture/server"
	"surl/internal/infrasturcture/storage"
	"surl/pkg/httpport"

	"github.com/go-chi/chi/v5"
	"github.com/go-openapi/runtime/middleware"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog/log"
)

type Config struct {
	OpenApiDocPath string `split_words:"true"`
}

func main() {
	logger := log.Logger

	cfg := &Config{}
	if err := envconfig.Process("server", cfg); err != nil {
		logger.Fatal().Err(err).Msg("server bootstrap: parse config")
	}

	rdb, err := storage.NewRedisClient(nil)
	if err != nil {
		logger.Fatal().Err(err).Msg("server bootstrap: setup redis client")
	}

	shortUrlStorage := storage.NewRedisShortUrlStorage(rdb)
	shortUrlService, err := short_url.New(shortUrlStorage)
	if err != nil {
		logger.Fatal().Err(err).Msg("server bootstrap: setup short url service")
	}

	httpServer, err := app.NewHttpServer(logger, shortUrlService)
	if err != nil {
		logger.Fatal().Err(err).Msg("server bootstrap: setup http server application")
	}

	httpport.RunChiServer(nil, logger, func(r chi.Router) http.Handler {
		r.Use(httpport.NewRequestLogger(logger), httpport.NewLoggingRecoverer(logger))
		handleDocs(r, cfg)

		return server.HandlerWithOptions(httpServer, server.ChiServerOptions{
			BaseRouter: r,
			ErrorHandlerFunc: func(w http.ResponseWriter, r *http.Request, err error) {
				w.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(w).Encode(domain.Error{Message: err.Error()})
			},
		})
	})
}

func handleDocs(r chi.Router, cfg *Config) {
	opt := middleware.RedocOpts{
		SpecURL: "/surl-openapi",
		Path:    "/doc",
	}

	r.Handle("/doc", middleware.Redoc(opt, nil))
	r.Handle("/surl-openapi", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		http.ServeFile(w, req, cfg.OpenApiDocPath)
	}))
}
