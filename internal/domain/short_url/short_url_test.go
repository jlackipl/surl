package short_url_test

import (
	"context"
	"surl/internal/domain"
	"surl/internal/domain/short_url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type spyStorage struct {
	mock.Mock
}

func (s *spyStorage) Persist(ctx context.Context, url domain.Url) error {
	args := s.Called(ctx, url)

	return args.Error(0)
}

func (s *spyStorage) GetAll(ctx context.Context) ([]domain.Url, error) {
	args := s.Called(ctx)

	return args.Get(0).([]domain.Url), args.Error(1)
}

func (s *spyStorage) Get(ctx context.Context, id string) (domain.Url, error) {
	args := s.Called(ctx, id)

	return args.Get(0).(domain.Url), args.Error(1)
}

func (s *spyStorage) Delete(ctx context.Context, id string) error {
	args := s.Called(ctx, id)

	return args.Error(0)
}

func (s *spyStorage) IncrementCounter(ctx context.Context, id string) error {
	args := s.Called(ctx, id)

	return args.Error(0)
}

func Test_Create(t *testing.T) {
	cmd := domain.CreateUrl{Url: "http://test.com/"}

	spyStorage := new(spyStorage)
	spyStorage.On("Persist", mock.Anything, mock.MatchedBy(func(url domain.Url) bool {
		return cmd.Url == url.Url && len(url.Id) > 0
	})).Return(nil)

	service, err := short_url.New(spyStorage)
	assert.NoError(t, err)

	shortUrl, err := service.Create(context.Background(), cmd)

	assert.NoError(t, err)
	assert.NotEmpty(t, shortUrl.Id)
	assert.Equal(t, cmd.Url, shortUrl.Url)
	spyStorage.AssertExpectations(t)
}

func Test_Create_With_Invalid_Data(t *testing.T) {
	type (
		testcase struct {
			name string

			cmd domain.CreateUrl

			expectedError error
		}
	)

	tc := []testcase{
		{
			name: "empty url",
			cmd:  domain.CreateUrl{},
			expectedError: short_url.InvalidDataError{
				Message: "url cannot be empty",
			},
		},
		{
			name: "invalid url format: no schema",
			cmd: domain.CreateUrl{
				Url: "/",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: missing protocol scheme",
			},
		},
		{
			name: "invalid url format: no schema",
			cmd: domain.CreateUrl{
				Url: "/login",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: missing protocol scheme",
			},
		},
		{
			name: "invalid url format: no schema",
			cmd: domain.CreateUrl{
				Url: "//google.com",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: missing protocol scheme",
			},
		},
		{
			name: "invalid url format: no schema",
			cmd: domain.CreateUrl{
				Url: "google.com",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: missing protocol scheme",
			},
		},
		{
			name: "invalid url format: no schema",
			cmd: domain.CreateUrl{
				Url: "http:google.com",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: missing host",
			},
		},
		{
			name: "invalid url format",
			cmd: domain.CreateUrl{
				Url: "://google.com",
			},
			expectedError: short_url.InvalidDataError{
				Message: "invalid url format: parse \"://google.com\": missing protocol scheme",
			},
		},
	}

	for _, c := range tc {
		t.Run(c.name, func(t *testing.T) {
			spyStorage := new(spyStorage)

			service, err := short_url.New(spyStorage)
			assert.NoError(t, err)

			shortUrl, err := service.Create(context.Background(), c.cmd)

			assert.Zero(t, shortUrl)
			assert.ErrorIs(t, err, c.expectedError)
			spyStorage.AssertExpectations(t)
		})
	}
}
