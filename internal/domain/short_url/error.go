package short_url

import "fmt"

type UrlNotFoundErr struct {
	Id string
}

func (e UrlNotFoundErr) Error() string {
	return fmt.Sprintf("url '%s' do not exist", e.Id)
}

type InvalidDataError struct {
	Message string
}

func (e InvalidDataError) Error() string {
	return e.Message
}
