package short_url

import (
	"context"
	"fmt"
	"net/url"
	"surl/internal/domain"
	"surl/pkg/shortener"
)

type ShortUrlService struct {
	storage Storage
}

type Storage interface {
	// Persist store object into storage. If object already exist under with same Id then do nothing.
	Persist(ctx context.Context, url domain.Url) error
	GetAll(ctx context.Context) ([]domain.Url, error)
	Get(ctx context.Context, id string) (domain.Url, error)
	IncrementCounter(ctx context.Context, id string) error
	Delete(ctx context.Context, id string) error
}

func New(storage Storage) (*ShortUrlService, error) {
	return &ShortUrlService{storage: storage}, nil
}

// Create function generate shorter string based on cmd.Url and store in storage.
func (s *ShortUrlService) Create(ctx context.Context, cmd domain.CreateUrl) (domain.Url, error) {
	url, err := parseUrl(cmd.Url)
	if err != nil {
		return domain.Url{}, err
	}

	shortUrl := domain.Url{Id: shortener.GenerateShortLink(url.String()), Url: url.String()}
	if err := s.storage.Persist(ctx, shortUrl); err != nil {
		return domain.Url{}, fmt.Errorf("%s: %w", "create short url", err)
	}

	return shortUrl, nil
}

func parseUrl(stringUrl string) (*url.URL, error) {
	if stringUrl == "" {
		return nil, fmt.Errorf("parse url: %w", InvalidDataError{Message: "url cannot be empty"})
	}

	u, err := url.Parse(stringUrl)
	if err != nil {
		return nil, fmt.Errorf("parse url: %w", InvalidDataError{Message: "invalid url format: " + err.Error()})
	}

	if u.Scheme == "" {
		return nil, fmt.Errorf("parse url: %w", InvalidDataError{Message: "invalid url format: missing protocol scheme"})
	}

	if u.Host == "" {
		return nil, fmt.Errorf("parse url: %w", InvalidDataError{Message: "invalid url format: missing host"})
	}

	return u, nil
}

// GetAll return list of domain.Url stored in the system
func (s *ShortUrlService) GetAll(ctx context.Context) ([]domain.Url, error) {
	return s.storage.GetAll(ctx)
}

// Get return domain.Url stored under provided id.
func (s *ShortUrlService) Get(ctx context.Context, id string) (domain.Url, error) {
	return s.storage.Get(ctx, id)
}

// IncrementCounter increment url counter by 1
func (s *ShortUrlService) IncrementCounter(ctx context.Context, id string) error {
	return s.storage.IncrementCounter(ctx, id)
}

// Delete remove domain.Url from storage.
func (s *ShortUrlService) Delete(ctx context.Context, id string) error {
	return s.storage.Delete(ctx, id)
}
