package storage_test

import (
	"context"
	"surl/internal/domain"
	"surl/internal/domain/short_url"
	"surl/internal/infrasturcture/storage"
	"testing"

	redishelper "github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func Test_Persist(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushDB(context.Background())

	url := domain.Url{Id: uuid.NewString(), Url: "http://test.com/"}
	service := storage.NewRedisShortUrlStorage(rdb)

	err = service.Persist(context.Background(), url)
	assert.NoError(t, err)

	var result storage.UrlReadModel
	err = rdb.HGetAll(context.Background(), storage.UrlRootKey+url.Id).Scan(&result)
	assert.NoError(t, err)
	assert.Equal(t, url, result.ToDomainUrl())
}

func Test_Get(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushAll(context.Background())

	url := domain.Url{Id: uuid.NewString(), Url: "http://test.com/"}
	err = rdb.HMSet(context.Background(), storage.UrlRootKey+url.Id, redishelper.Args{}.AddFlat(storage.NewUrlReadModel(url))...).Err()
	assert.NoError(t, err)

	service := storage.NewRedisShortUrlStorage(rdb)

	result, err := service.Get(context.Background(), url.Id)
	assert.NoError(t, err)
	assert.Equal(t, url, result)
}

func Test_Get_not_existed_url(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushAll(context.Background())

	service := storage.NewRedisShortUrlStorage(rdb)

	id := uuid.NewString()
	result, err := service.Get(context.Background(), id)
	assert.ErrorIs(t, err, short_url.UrlNotFoundErr{Id: id})
	assert.Zero(t, result)
}

func Test_Delte(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushDB(context.Background())

	url := domain.Url{Id: uuid.NewString(), Url: "http://test.com/"}
	err = rdb.HMSet(context.Background(), storage.UrlRootKey+url.Id, redishelper.Args{}.AddFlat(storage.NewUrlReadModel(url))...).Err()
	assert.NoError(t, err)

	service := storage.NewRedisShortUrlStorage(rdb)

	err = service.Delete(context.Background(), url.Id)
	assert.NoError(t, err)
	assert.Equal(t, 0, int(rdb.Exists(context.Background(), storage.UrlRootKey+url.Id).Val()))
}

func Test_Delete_not_existed_url(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushDB(context.Background())

	service := storage.NewRedisShortUrlStorage(rdb)

	id := uuid.NewString()
	err = service.Delete(context.Background(), id)
	assert.ErrorIs(t, err, short_url.UrlNotFoundErr{Id: id})
}

func Test_Increment(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushDB(context.Background())

	url := domain.Url{Id: uuid.NewString(), Url: "http://test.com/"}
	err = rdb.HMSet(context.Background(), storage.UrlRootKey+url.Id, redishelper.Args{}.AddFlat(storage.NewUrlReadModel(url))...).Err()
	assert.NoError(t, err)

	service := storage.NewRedisShortUrlStorage(rdb)

	err = service.IncrementCounter(context.Background(), url.Id)
	assert.NoError(t, err)

	var result storage.UrlReadModel
	err = rdb.HGetAll(context.Background(), storage.UrlRootKey+url.Id).Scan(&result)
	assert.NoError(t, err)
	assert.Equal(t, 1, result.Counter)
}

func Test_Increment_not_existed_url(t *testing.T) {
	rdb, err := storage.NewRedisClient(nil)
	assert.NoError(t, err)
	defer rdb.FlushDB(context.Background())

	service := storage.NewRedisShortUrlStorage(rdb)

	id := uuid.NewString()
	err = service.IncrementCounter(context.Background(), id)
	assert.ErrorIs(t, err, short_url.UrlNotFoundErr{Id: id})
}
