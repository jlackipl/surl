package storage

import (
	"context"
	"fmt"
	"surl/internal/domain"
	"surl/internal/domain/short_url"

	"github.com/go-redis/redis/v8"

	redishelper "github.com/gomodule/redigo/redis"
)

const UrlRootKey = "url-"

type UrlReadModel struct {
	Counter int    `redis:"counter"`
	Id      string `redis:"id"`
	Url     string `redis:"url"`
}

func NewUrlReadModel(url domain.Url) UrlReadModel {
	return UrlReadModel{
		Counter: url.Counter,
		Id:      url.Id,
		Url:     url.Url,
	}
}

func (u UrlReadModel) ToDomainUrl() domain.Url {
	return domain.Url{
		Counter: u.Counter,
		Id:      u.Id,
		Url:     u.Url,
	}
}

type RedisShortUrlStorage struct {
	rdb *redis.Client
}

func NewRedisShortUrlStorage(rdb *redis.Client) *RedisShortUrlStorage {
	return &RedisShortUrlStorage{rdb: rdb}
}

func (r *RedisShortUrlStorage) Persist(ctx context.Context, url domain.Url) error {
	keys, err := r.rdb.Keys(ctx, UrlRootKey+url.Id).Result()
	if err != nil {
		return err
	}
	if len(keys) > 0 {
		return nil
	}

	result := r.rdb.HMSet(ctx, UrlRootKey+url.Id, redishelper.Args{}.AddFlat(NewUrlReadModel(url))...)

	return result.Err()
}

func (r *RedisShortUrlStorage) GetAll(ctx context.Context) ([]domain.Url, error) {
	var url UrlReadModel

	keys, err := r.rdb.Keys(ctx, UrlRootKey+"*").Result()
	if err != nil {
		return nil, err
	}

	urls := make([]domain.Url, len(keys))
	for i, k := range keys {
		result := r.rdb.HGetAll(ctx, k)
		if err := result.Scan(&url); err != nil {
			return nil, fmt.Errorf("get ulr: cannot deserialize data: %w", err)
		}

		urls[i] = url.ToDomainUrl()
	}

	return urls, nil
}

func (r *RedisShortUrlStorage) Get(ctx context.Context, id string) (domain.Url, error) {
	var url UrlReadModel

	result := r.rdb.HGetAll(ctx, UrlRootKey+id)
	if len(result.Val()) == 0 {
		return domain.Url{}, short_url.UrlNotFoundErr{Id: id}
	}

	if err := result.Scan(&url); err != nil {
		return domain.Url{}, fmt.Errorf("get ulr: cannot deserialize data: %w", err)
	}

	return url.ToDomainUrl(), nil
}

func (r *RedisShortUrlStorage) Delete(ctx context.Context, id string) error {
	result, err := r.rdb.Del(ctx, UrlRootKey+id).Result()
	if err != nil {
		return fmt.Errorf("delete error: %w", err)
	}

	if result == 0 {
		return short_url.UrlNotFoundErr{Id: id}
	}

	return nil
}

func (r *RedisShortUrlStorage) IncrementCounter(ctx context.Context, id string) error {
	if r.rdb.Exists(ctx, UrlRootKey+id).Val() == 0 {
		return short_url.UrlNotFoundErr{Id: id}
	}

	r.rdb.Type(ctx, id)
	_, err := r.rdb.HIncrBy(ctx, UrlRootKey+id, "counter", 1).Result()
	if err != nil {
		return fmt.Errorf("increment counter error: %w", err)
	}

	return nil
}
