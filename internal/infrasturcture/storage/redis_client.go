package storage

import (
	"fmt"
	"net/url"

	"github.com/go-redis/redis/v8"
	"github.com/kelseyhightower/envconfig"
)

type RedisConfig struct {
	Host     string
	Port     string `default:"6379"`
	User     string
	Password string
}

func NewRedisClient(cfg *RedisConfig) (*redis.Client, error) {
	if cfg == nil {
		cfg = &RedisConfig{}
		if err := envconfig.Process("redis", cfg); err != nil {
			return nil, fmt.Errorf("read server config: %w", err)
		}
	}

	return redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		Password: url.QueryEscape(cfg.Password),
	}), nil
}
