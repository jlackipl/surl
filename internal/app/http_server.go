package app

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"surl/internal/domain"
	"surl/internal/domain/short_url"
	"surl/pkg/httpresponse"

	"github.com/rs/zerolog"
)

type ShortUrlService interface {
	Create(ctx context.Context, cmd domain.CreateUrl) (domain.Url, error)
	GetAll(ctx context.Context) ([]domain.Url, error)
	Get(ctx context.Context, id string) (domain.Url, error)
	IncrementCounter(ctx context.Context, id string) error
	Delete(ctx context.Context, id string) error
}

type HttpServer struct {
	logger          zerolog.Logger
	shortUrlService ShortUrlService
}

func NewHttpServer(logger zerolog.Logger, shortUrlService ShortUrlService) (*HttpServer, error) {
	return &HttpServer{logger: logger, shortUrlService: shortUrlService}, nil
}

func (s *HttpServer) GetUrls(w http.ResponseWriter, r *http.Request) {
	urls, err := s.shortUrlService.GetAll(r.Context())
	if err != nil {
		httpresponse.Respone(w, domain.Error{Message: err.Error()}, http.StatusInternalServerError)
		return
	}

	httpresponse.Respone(w, urls, http.StatusOK)
}

// (POST /api/urls)
func (s *HttpServer) CreateUrl(w http.ResponseWriter, r *http.Request) {
	var createUrl domain.CreateUrl
	if err := json.NewDecoder(r.Body).Decode(&createUrl); err != nil {
		s.logger.Info().Msgf("create short url: invalid request body: %v", err)
		httpresponse.Respone(w, domain.Error{Message: "invalid json body"}, http.StatusBadRequest)
		return
	}

	url, err := s.shortUrlService.Create(r.Context(), createUrl)
	if err != nil {
		var invalidDataErr short_url.InvalidDataError
		if errors.As(err, &invalidDataErr) {
			httpresponse.Respone(w, domain.Error{Message: invalidDataErr.Error()}, http.StatusBadRequest)
			return
		}

		s.logger.Err(err).Msg("create short url error")

		httpresponse.Respone(w, domain.Error{Message: "create short url: internal server error"}, http.StatusInternalServerError)
		return
	}

	httpresponse.Respone(w, url, http.StatusCreated)
}

// (DELETE /api/urls/{id})
func (s *HttpServer) DeleteUrl(w http.ResponseWriter, r *http.Request, id string) {
	err := s.shortUrlService.Delete(r.Context(), id)
	if err != nil {
		var notFound short_url.UrlNotFoundErr
		if errors.As(err, &notFound) {
			httpresponse.Respone(w, domain.Error{Message: notFound.Error()}, http.StatusNotFound)
			return
		}
	}

	httpresponse.Respone(w, []byte{}, http.StatusNoContent)
}

// (GET /api/urls/{id})
func (s *HttpServer) GetUrl(w http.ResponseWriter, r *http.Request, id string) {
	url, err := s.shortUrlService.Get(r.Context(), id)
	if err != nil {
		var notFound short_url.UrlNotFoundErr
		if errors.As(err, &notFound) {
			httpresponse.Respone(w, domain.Error{Message: notFound.Error()}, http.StatusNotFound)
			return
		}

		httpresponse.Respone(w, domain.Error{Message: err.Error()}, http.StatusInternalServerError)
		return
	}

	httpresponse.Respone(w, url, http.StatusOK)
}

// (GET /{urlId})
func (s *HttpServer) RedirectShortURL(w http.ResponseWriter, r *http.Request, urlId string) {
	shortUrl, err := s.shortUrlService.Get(r.Context(), urlId)
	if err != nil {
		var notFound short_url.UrlNotFoundErr
		if errors.As(err, &notFound) {
			httpresponse.Respone(w, domain.Error{Message: notFound.Error()}, http.StatusNotFound)
			return
		}

		httpresponse.Respone(w, domain.Error{Message: err.Error()}, http.StatusInternalServerError)
		return
	}

	s.shortUrlService.IncrementCounter(r.Context(), shortUrl.Id)
	http.Redirect(w, r, shortUrl.Url, http.StatusFound)
}
