package app_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"surl/internal/app"
	"surl/internal/domain"
	"surl/pkg/shortener"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type spyShortUrlService struct {
	mock.Mock
}

func (s *spyShortUrlService) Create(ctx context.Context, cmd domain.CreateUrl) (domain.Url, error) {
	args := s.Called(ctx, cmd)

	return args.Get(0).(domain.Url), args.Error(1)
}

func (s *spyShortUrlService) GetAll(ctx context.Context) ([]domain.Url, error) {
	args := s.Called(ctx)

	return args.Get(0).([]domain.Url), args.Error(1)
}

func (s *spyShortUrlService) Get(ctx context.Context, id string) (domain.Url, error) {
	args := s.Called(ctx, id)

	return args.Get(0).(domain.Url), args.Error(1)
}

func (s *spyShortUrlService) IncrementCounter(ctx context.Context, id string) error {
	args := s.Called(ctx, id)

	return args.Error(0)
}

func (s *spyShortUrlService) Delete(ctx context.Context, id string) error {
	args := s.Called(ctx, id)

	return args.Error(0)
}

func Test_CreateUrl(t *testing.T) {
	cmd := domain.CreateUrl{Url: "google.com"}
	expoectedShortUrl := domain.Url{
		Id:  shortener.GenerateShortLink(cmd.Url),
		Url: cmd.Url,
	}

	shortUrlService := new(spyShortUrlService)
	shortUrlService.
		On("Create", mock.Anything, cmd).
		Return(expoectedShortUrl, nil)

	hs, err := app.NewHttpServer(zerolog.Nop(), shortUrlService)
	assert.NoError(t, err)

	body := strings.NewReader(`{ "url": "google.com" }`)
	recorder := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPost, "", body)

	hs.CreateUrl(recorder, req)

	var response domain.Url
	assert.NoError(t, json.NewDecoder(recorder.Body).Decode(&response))
	assert.Equal(t, http.StatusCreated, recorder.Result().StatusCode)
	assert.Equal(t, expoectedShortUrl, response)
	shortUrlService.AssertExpectations(t)
}

func Test_CreateUrl_Invalid_request_body(t *testing.T) {
	type (
		testcase struct {
			name string
			body string

			expectedStatusCode int
		}
	)

	tc := []testcase{
		{
			name:               "empty body",
			body:               "",
			expectedStatusCode: http.StatusBadRequest,
		},
		{
			name:               "not valid json",
			body:               `"url": "https://google.com/"`,
			expectedStatusCode: http.StatusBadRequest,
		},
		{
			name:               "not valid json",
			body:               `{ "url": "https://google.com/} `,
			expectedStatusCode: http.StatusBadRequest,
		},
	}

	for _, c := range tc {
		t.Run(c.name, func(t *testing.T) {
			shortUrlService := new(spyShortUrlService)

			hs, err := app.NewHttpServer(zerolog.Nop(), shortUrlService)
			assert.NoError(t, err)

			body := strings.NewReader(c.body)
			recorder := httptest.NewRecorder()
			req, err := http.NewRequest(http.MethodPost, "", body)

			hs.CreateUrl(recorder, req)

			assert.Equal(t, c.expectedStatusCode, recorder.Result().StatusCode)
			shortUrlService.AssertExpectations(t)
		})
	}
}
