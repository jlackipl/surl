# SURL - Short URL API

<div align="center" width="100%">
    <img src="./logo.png"/>
</div>

## Requirements
Before start make sure to meet all requirements from the list:
1. Enusre that you could execute Makefile
2. Install docker
3. Install go1.18 - if you want to run test and for local development

## API Doc
Api doc is written in openapi spec here: `surl-openapi.yaml` also this doc will be serve as redoc website under url here: 
`http://localhost:8000/doc`

## Setup
SURL can be fully operated by using Makefile commands. To check all available commands execute following command in project root direcotry:
```
make help
```
For more understaning please look into Makefile.

### Setup for local development:
First of all make sure to build docker image for local development:
```
make docker-image
```
install all dependencies:
```
make vendor
```

## Run localy
Api and database will be run inside docker containers.

In order to run SURL service locally:
```
// Create and run database 
make db
// Run application
make run
```
Container with database run in detach mode so we don't need to run it each time when we want to perform test or run/restart application.
But if we want to clear database we need to run `make db-clean`. And that remove container and to use it again we have to run it by `make db` once more.

## Run test
In order to perform test locally execute following commands:
```
// Create and run database 
make db
// Run test
make test
```
