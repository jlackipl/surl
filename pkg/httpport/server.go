package httpport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog"
)

type Config struct {
	Port           int      `default:"8000"`
	AllowedOrigins []string `default:"*"`
}

func RunChiServer(cfg *Config, logger zerolog.Logger, createHandler func(chi.Router) http.Handler) error {
	if cfg == nil {
		cfg = &Config{}
		if err := envconfig.Process("server", cfg); err != nil {
			return fmt.Errorf("read server config: %v", err)
		}
	}

	apiRouter := chi.NewRouter()
	apiRouter.Use(
		newCORS(cfg.AllowedOrigins),
	)

	logger.Info().Int("port", cfg.Port).Msg("server listening")
	return http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), createHandler(apiRouter))
}

func newCORS(allowedOrigins []string) func(http.Handler) http.Handler {
	opts := cors.Options{
		AllowedOrigins: allowedOrigins,
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		},
		AllowedHeaders: []string{"*"},
		MaxAge:         300,
	}
	return cors.New(opts).Handler
}
