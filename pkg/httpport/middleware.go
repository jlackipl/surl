package httpport

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog"
)

func NewRequestLogger(logger zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			start := time.Now()
			defer func() {
				path := r.URL.Path
				status := ww.Status()

				logger.Info().
					Str("requestId", middleware.GetReqID(r.Context())).
					Str("method", r.Method).
					Str("path", path).
					Int("status", status).
					Dur("duration", time.Since(start)).
					Msg("request completed")
			}()

			next.ServeHTTP(ww, r)
		})
	}
}

func NewLoggingRecoverer(logger zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if re := recover(); re != nil && re != http.ErrAbortHandler {
					err, ok := re.(error)
					if !ok {
						err = fmt.Errorf("%v", re)
					}

					logger.
						WithLevel(zerolog.PanicLevel).
						Err(err).
						Str("requestId", middleware.GetReqID(r.Context())).
						Msg("")

					w.WriteHeader(http.StatusInternalServerError)
				}
			}()

			next.ServeHTTP(w, r)
		})
	}
}
