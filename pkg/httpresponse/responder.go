package httpresponse

import (
	"encoding/json"
	"net/http"
)

func Respone(w http.ResponseWriter, v any, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	if err := json.NewEncoder(w).Encode(v); err != nil {
		panic(err)
	}
}
