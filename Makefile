APP_NAME=surl

## DOCKER config
DOCKER_RUNNER=docker run -u :$(shell id -g ${USER}) -v $(shell pwd):/surl -w /surl --network host ${APP_NAME}-builder 

## GO config
GO=go
GO_TOOL=${GO} tool
GO_INSTALL=${GO} install
GO_VENDOR=${GO} mod vendor
GO_TEST=${GO} test -coverpkg=./... -coverprofile coverage.out ./...
GO_COVERAGE=${GO_TOOL} cover -func=coverage.out
GO_RUN=${GO} run

## OPENAPI config
OAPI_DOC=surl-openapi.yaml
OAPI_SERVER_TYPE=chi-server
OAPI_OUTPUT_PATH=internal

OAPI_SERVER_PKG_NAME=server
OAPI_SERVER_OUTPUT_PATH=${OAPI_OUTPUT_PATH}/infrasturcture/${OAPI_SERVER_PKG_NAME}
OAPI_SERVER_FILE_NAME=server.gen.go

OAPI_CLIENT_PKG_NAME=surl_sdk
OAPI_CLIENT_OUTPUT_PATH=pkg/${OAPI_CLIENT_PKG_NAME}
OAPI_CLIENT_FILE_NAME=surl_http_client.gen.go
OAPI_CLIENT_TYPES_FILE_NAME=surl_types.gen.go

OAPI_TYPES_PKG_NAME=domain
OAPI_TYPES_OUTPUT_PATH=${OAPI_OUTPUT_PATH}/${OAPI_TYPES_PKG_NAME}
OAPI_TYPES_FILE_NAME=types.gen.go

## DB config
DB_CONTAINER="surl_db"
DB_PORT?=6379

## APP env
PORT?=8000
ENV=SERVER_PORT=${PORT} \
	SERVER_OPEN_API_DOC_PATH=${OAPI_DOC} \
	REDIS_HOST="" \
	REDIS_PORT=${DB_PORT}

##@ Local development
.PHONY: docker-image
docker-image: ## Build docker image
	@docker build -f infrastructure/docker/Dockerfile --target builder -t ${APP_NAME}-builder .

.PHONY: oapi-gen
oapi-gen:: oapi-server oapi-types ## Generate server and types based on openapi file

.PHONY: oapi-server
oapi-server: ## Generate server based on openapi file
	@${DOCKER_RUNNER} mkdir -p ${OAPI_SERVER_OUTPUT_PATH}
	@${DOCKER_RUNNER} oapi-codegen -generate ${OAPI_SERVER_TYPE} -o ${OAPI_SERVER_OUTPUT_PATH}/${OAPI_SERVER_FILE_NAME} -package ${OAPI_SERVER_PKG_NAME} ${OAPI_DOC}

.PHONY: oapi-types
oapi-types: ## Generate types based on openapi file
	@${DOCKER_RUNNER} mkdir -p ${OAPI_TYPES_OUTPUT_PATH}
	@${DOCKER_RUNNER} oapi-codegen -generate types -o ${OAPI_TYPES_OUTPUT_PATH}/${OAPI_TYPES_FILE_NAME} -package ${OAPI_TYPES_PKG_NAME} ${OAPI_DOC}

.PHONY: vendor
vendor: ## Download dependencies
	@${DOCKER_RUNNER} ${GO_VENDOR}

.PHONY: test
test: ## Run tests
	@${ENV} ${GO_TEST}
	@${GO_COVERAGE}

.PHONY: run
run: ## Run app
	@${DOCKER_RUNNER} sh -c `${ENV} ${GO_RUN} ./cmd/server/main.go`

.PHONY: db
db: ## Run db
	@docker run -d --name ${DB_CONTAINER} -p ${DB_PORT}:${DB_PORT} --network host redis

.PHONY: db-clean
db-clean: ## Remove db container
	@docker stop ${DB_CONTAINER}
	@docker container rm ${DB_CONTAINER}

##@ Help
.PHONY: help
help: ## Display Makefile help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make [STEP] [ENV_VARIALBE=value, ...; optional] \033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
